<?php

use App\Http\Controllers\InicioController;
use App\Http\Controllers\GasolineraController;
use App\Http\Controllers\ComentarioController;
use App\Http\Controllers\FavoritoController;
use App\Http\Controllers\RepostajeController;
use App\Http\Controllers\PuntuacionController;
use App\Http\Controllers\CalculadoraController;
use App\Http\Controllers\CombustibleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class,'inicio'])->name('home');

Route::get('gasolineras', [GasolineraController::class,'index'])->name('gasolineras.index');

Route::get('gasolineras/{gasolinera}', [GasolineraController::class,'show'])->name('gasolineras.show');

Route::post('comentarios/insertar', [ComentarioController::class, 'insertarComentario'])->name('comentarios.insertar');

Route::post('comentarios/modificar', [ComentarioController::class, 'modificarComentario'])->name('comentarios.modificar');

Route::post('comentarios/visibilidad', [ComentarioController::class, 'visibilidadComentario'])->name('comentarios.visibilidad');

//Route::get('favoritos', [FavoritoController::class,'index'])->name('favoritos.index');

Route::middleware(['auth:sanctum', 'verified'])->get('favoritos', [FavoritoController::class,'index'])->name('favoritos.index');

Route::post('favoritos/borrar', [FavoritoController::class, 'borrarFavorito'])->name('favoritos.borrar');

Route::post('favoritos/insertar', [FavoritoController::class, 'insertarFavorito'])->name('favoritos.insertar');

Route::middleware(['auth:sanctum', 'verified'])->get('repostajes', [RepostajeController::class,'index'])->name('repostajes.index');

Route::post('repostajes', [RepostajeController::class,'store'])->name("repostajes.store");

Route::post('repostajes/actualizar',[RepostajeController::class,'update'])->name("repostajes.update");

Route::middleware(['auth:sanctum', 'verified'])->get('repostajes/{id}/editar', [RepostajeController::class,'edit'])->name('repostajes.edit');

Route::middleware(['auth:sanctum', 'verified'])->get('repostajes/{id}/crear', [RepostajeController::class,'create'])->name('repostajes.create');

Route::post('puntuaciones/insertar', [PuntuacionController::class, 'insertarPuntuacion'])->name('puntuaciones.insertar');

Route::post('puntuaciones/modificar', [PuntuacionController::class, 'modificarPuntuacion'])->name('puntuaciones.modificar');

Route::get('calculadora', [CalculadoraController::class,'index'])->name('calculadoras.index');

Route::get('combustibles', [CombustibleController::class,'index'])->name('combustibles.index');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
