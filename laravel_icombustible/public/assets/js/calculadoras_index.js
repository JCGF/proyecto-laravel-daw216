$( document ).ready(function() {

  // Limpiar las cajas
  borrarInputs();
  
  $("#bCalcularI").click(function(){
    // Declarar variables
    let litrosRepostados = $("#cLitrosRepostados").val();
    let precioPorLitro = $("#cPrecioPorLitro").val();

    // Realizar validaciones
    if (litrosRepostados != "" && !isNaN(litrosRepostados)) {
      
      if (precioPorLitro != "" && !isNaN(precioPorLitro)) {
      
        // Realizar calculo
        let importeTotal = parseFloat(litrosRepostados) * parseFloat(precioPorLitro);
        // Fijar el resultado con dos decimales
        importeTotal = importeTotal.toFixed(2);

        // Rellenar el Importe
        $('#cImporteTotal').val(importeTotal.replace(".", ","));

      } else {
        alert("Por favor, inserte un valor válido en precio por litro.");
        // Limpiar valores
        $("#cPrecioPorLitro").val('');
        $('#cImporteTotal').val('');
      }

    } else {
      alert("Por favor, inserte un valor válido en litros repostados.");
      // Limpiar valores
      $("#cLitrosRepostados").val('');
      $('#cImporteTotal').val('');
    }

  });

  
  function borrarInputs() {
    // Limpiar valores
    $("#cLitrosRepostados").val('');
    $("#cPrecioPorLitro").val('');
    $("#cImporteTotal").val('');    
  }

});