$( document ).ready(function() {

  // Declarar constantes
  const usuarioId = $.trim( $("#userId").val() );
  const gasolineraId = $.trim( $("#gasolineraId").val() );

  /*
    FAVORITO
  */
  $("#favoritoCorazon").click(function(){

    // Comprobar el estado del corazon
    if (this.dataset.corazon == "vacio") {
      
      // Peticion POST Ajax
      $.ajax({
        url: '/favoritos/insertar',
        type: "POST",
        dataType: 'html',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        data: { 
          user_id: usuarioId,
          gasolinera_id: gasolineraId 
        }
      }).done(function(response) {
        // alert("Favorito añadido con éxito.");

        // Recargar la web
        location.reload();
      })
      .fail(function(response) {
        // Error al crear el favorito
        alert("¡Error! No se ha podido añadir como favorito.");
      });

    } else {
      
      // Peticion POST Ajax
      $.ajax({
        url: '/favoritos/borrar',
        type: "POST",
        dataType: 'html',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        data: { 
          user_id: usuarioId,
          gasolinera_id: gasolineraId 
        }
      }).done(function(response) {
        // alert("Favorito borrado con éxito.");

        // Recargar la web
        location.reload();
      })
      .fail(function(response) {
        // Error al borrar el favorito
        alert("¡Error! No se ha podido borrar de favoritos.");
      });
    }

  });  

  /*
    PUNTUACION
  */
  $("#botonVotar").click(function(){
    
    // Declarar variable
    let valorV = $("input[name='inlineRadioOptions']:checked").val();

    // Comprobar el estado del boton
    if (this.dataset.existev == "no") {

      // Peticion POST Ajax
      $.ajax({
        url: '/puntuaciones/insertar',
        type: "POST",
        dataType: 'html',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        data: { 
          valor: valorV, 
          user_id: usuarioId,
          gasolinera_id: gasolineraId
        }
      }).done(function(response) {
        // Mensaje voto añadido 
        alert("Voto añadido con éxito.");

        // Recargar la web
        location.reload();
      })
      .fail(function(response) {
        // Error al añadir el voto
        alert("¡Error! No se ha podido añadir el voto.");
      });

    } else {

      // Peticion POST Ajax
      $.ajax({
        url: '/puntuaciones/modificar',
        type: "POST",
        dataType: 'html',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        data: { 
          valor: valorV, 
          user_id: usuarioId,
          gasolinera_id: gasolineraId
        }
      }).done(function(response) {
        // Mensaje voto modificado
        alert("Voto modificado con éxito.");

        // Recargar la web
        location.reload();
      })
      .fail(function(response) {
        // Error al modificar el voto
        alert("¡Error! No se ha podido modificar el voto.");
      });

    }

  });

  /*
    COMENTARIOS 
  */

  // Limpiar la caja de comentarios
  $("#textoComentario").val('');

  /*
    PUBLICAR NUEVO COMENTARIO 
  */

  $("#publicarComentario").click(function(){
    // Declarar variables
    let textoC = $.trim( $("#textoComentario").val() );
    let datosB = $(this).data();

    // Validar que el comentario no esté vacío
    if (textoC != "") {

      // Peticion POST Ajax
      $.ajax({
        url: '/comentarios/insertar',
        type: "POST",
        dataType: 'html',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        data: { 
          texto: textoC,
          visible: 1,
          user_id: datosB.usuarioid,
          gasolinera_id: datosB.gasolineraid	 
        }
      }).done(function(response) {
        // Mensaje comentario añadido
        // alert("Comentario registrado con éxito.");
        
        // Limpiar la caja de comentarios
        $("#textoComentario").val('');
        // Recargar la web
        location.reload();
      })
      .fail(function(response) {
        // Error al crear el comentario
        alert("¡Error! No se ha podido guardar el comentario.");
      });
    } else {
      // Mensaje comentario vacío
      alert("Por favor, no introduzca un comentario vacío.");
    }

  });

  /*
    MODIFICAR COMENTARIO 
  */
  
  // Buscar todos los botones
  $(".botonEditarC").each(function(){

    // Añadir comportamiento al hacer click
    $(this).on("click", function(){

      // Declarar variables
      let datosC = $(this).data(); 

      // Ocultar párrafo del comentario
      $("#pComentario" + datosC.comentarioid).hide();
      // Mostrar caja de edición del comentario
      $("#cajaComentario" + datosC.comentarioid).show();
      // Ocultar botón de editar el comentario
      $("#botonEditarC" + datosC.comentarioid).hide();
      // Ocultar botón de actualizar el comentario
      $("#botonActualizarC" + datosC.comentarioid).show();

      // Añadir comportamiento al hacer click
      $("#botonActualizarC" + datosC.comentarioid).on("click", function(){
        
        // Declarar variables
        let textoCO = $.trim( $("#pComentario" + datosC.comentarioid).html() );
        let textoCM = $.trim( $("#cajaComentario" + datosC.comentarioid).val() );

        // Comprobar si los dos textos son iguales
        if (textoCO === textoCM) {
          // Mensaje comentario igual
          alert("Por favor, modifique el comentario para que sea distinto al original.");
        } else {
          // Comprobar que el nuevo comentario no este vacío
          if (textoCM == "") {
            // Mensaje comentario vacío
            alert("Por favor, no introduzca un comentario vacío.");
          } else {

            // Peticion POST Ajax
            $.ajax({
              url: '/comentarios/modificar',
              type: "POST",
              dataType: 'html',
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
              data: { 
                comentario_id: datosC.comentarioid,
                texto: textoCM	 
              }
            }).done(function(response) {
              // Mensaje comentario modificado
              // alert("Comentario modificado con éxito.");
              
              // Recargar la web
              location.reload();
            })
            .fail(function(response) {
              // Error al modificar el comentario
              alert("¡Error! No se ha podido modificar el comentario.");
            });

          }
        }

      });  

    });

  });

  /*
    MODIFICAR VISIBILIDAD COMENTARIO 
  */
  
  // Buscar todos los botones
  $(".botonBorrarC").each(function(){

    // Añadir comportamiento al hacer click
    $(this).on("click", function(){
      
      // Declarar variables
      let datosC = $(this).data(); 

      // Preguntar al usuario si desea borrar el comentario
      if ( confirm("¿Deseas borrar de forma definitiva el comentario?")) {

        // Peticion POST Ajax
        $.ajax({
          url: '/comentarios/visibilidad',
          type: "POST",
          dataType: 'html',
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
          data: { 
            comentario_id: datosC.comentarioid	 
          }
        }).done(function(response) {
          // Mensaje comentario borrado
          // alert("Comentario borrado con éxito.");
          
          // Recargar la web
          location.reload();
        })
        .fail(function(response) {
          // Error al borrar el comentario
          alert("¡Error! No se ha podido borrar el comentario.");
        });
      }

    });  

  });  

});