<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precios', function (Blueprint $table) {
            $table->id();
            $table->float('importe', 4, 3);
            $table->unsignedBigInteger('combustible_id');
            $table->unsignedBigInteger('gasolinera_id');
            $table->timestamps();
            
            // Claves Foráneas
            $table->foreign('combustible_id')->references('id')->on('combustibles');
            $table->foreign('gasolinera_id')->references('id')->on('gasolineras');      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precios');
    }
}
