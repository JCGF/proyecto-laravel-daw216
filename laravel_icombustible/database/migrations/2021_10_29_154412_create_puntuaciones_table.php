<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePuntuacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puntuaciones', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('valor');
            $table->dateTime('fecha_puntuacion');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('gasolinera_id')->index();
            $table->timestamps();

            // Claves Foráneas
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('gasolinera_id')->references('id')->on('gasolineras');

            // Claves unicas
            $table->unique(['user_id', 'gasolinera_id']);     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puntuaciones');
    }
}
