<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCombustibleGasolineraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combustible_gasolinera', function (Blueprint $table) {   
            $table->unsignedBigInteger('combustible_id');
            $table->unsignedBigInteger('gasolinera_id');

            // Claves foráneas
            $table->foreign('combustible_id')->references('id')->on('combustibles');
            $table->foreign('gasolinera_id')->references('id')->on('gasolineras');
            // Clave primaria compuesta
            $table->primary(['combustible_id', 'gasolinera_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combustible_gasolinera');
    }
}
