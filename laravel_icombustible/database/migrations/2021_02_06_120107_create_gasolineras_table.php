<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGasolinerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasolineras', function (Blueprint $table) {
            $table->id();
            $table->string('rotulo');
            $table->string('slug')->unique();
            $table->string('latitud');
            $table->string('longitud');
            $table->char('margen', 1);
            $table->char('tipo_venta', 1);
            $table->string('direccion');
            $table->string('codigo_postal', 5);
            $table->string('localidad');
            $table->string('municipio');
            $table->string('provincia');
            $table->string('url_icono', 2048)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasolineras');
    }
}
