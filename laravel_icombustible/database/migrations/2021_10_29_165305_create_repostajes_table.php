<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepostajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repostajes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('gasolinera_id')->index();
            $table->double('importe');
            $table->double('litros_repostados');
            $table->date("fecha_repostaje");
            $table->timestamps();
                        
            // Claves Foráneas
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('gasolinera_id')->references('id')->on('gasolineras'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repostajes');
    }
}
