<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->id();
            $table->text('texto');
            $table->boolean('visible')->default(true);
            $table->dateTime('fecha_publicacion');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('gasolinera_id')->index();
            $table->timestamps();

            // Claves Foráneas
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('gasolinera_id')->references('id')->on('gasolineras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}
