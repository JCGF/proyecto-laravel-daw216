<?php

namespace Database\Seeders;

use App\Models\Combustible;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CombustibleGasolineraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $url_api = "https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/";
      $data = json_decode( file_get_contents($url_api), true );

      DB::beginTransaction();

      foreach ($data['ListaEESSPrecio'] as $gasolinera)
       {

         if( $gasolinera['Precio Gasolina 95 E5'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gasolina 95 E5')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }
        
         if( $gasolinera['Precio Gasolina 95 E10'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gasolina 95 E10')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }

         if( $gasolinera['Precio Gasolina 95 E5 Premium'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gasolina 95 E5 Premium')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }

         if( $gasolinera['Precio Gasolina 98 E5'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gasolina 98 E5')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }

         if( $gasolinera['Precio Gasolina 98 E10'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gasolina 98 E10')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }

         if( $gasolinera['Precio Gasoleo A'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gasóleo A')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }

         if( $gasolinera['Precio Gasoleo Premium'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gasóleo Premium')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }          

         if( $gasolinera['Precio Gasoleo B'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gasóleo B')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }
        
         if( $gasolinera['Precio Bioetanol'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Bioetanol')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }
         
         if( $gasolinera['Precio Biodiesel'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Biodiésel')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }

         if( $gasolinera['Precio Gases licuados del petróleo'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gases licuados del petróleo (GLP)')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }

         if( $gasolinera['Precio Gas Natural Comprimido'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gas natural comprimido (GNC)')->first()->id;

            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }

        if( $gasolinera['Precio Gas Natural Licuado'] != "")
          {
            // Id combustible
            $id_combustible = Combustible::where('nombre', 'Gas natural licuado (GNL)')->first()->id;
            DB::table('combustible_gasolinera')->insert([
              ['combustible_id' => $id_combustible, 'gasolinera_id' => $gasolinera['IDEESS']],
            ]);
          }

       }

      DB::commit();

      $this->command->info('Tabla combustible-gasolinera inicializada con datos.'); 
    }
    
}
