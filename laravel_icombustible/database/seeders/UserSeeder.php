<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

  private $usuarios = array(
    array(
      'name' => 'Administrador',
      'email' => 'admin@icombustible.com',
      'password' => '$2y$10$k6jBD6bmOsbBd9tsnwiUh.7nj/Z4GDQPlrHA5GEiGDY6dZBFhB9XW', // Administrador1234
      'rol_id' => '3'
    ),
    array(
      'name' => 'Moderador',
      'email' => 'mod@icombustible.com',
      'password' => '$2y$10$4.eDzXmkqrZybv3iw2hbUeU0Ai36hbzMTL7AzHt267UlWCPNd2xcS', // Moderador1234
      'rol_id' => '2'
    ),
    array(
      'name' => 'Juan Carlos González Fernández',
      'email' => 'juancarlos@icombustible.com',
      'password' => '$2y$10$WL9RPp1AT4a2E6UrAnFLnuTtAdleufOz6gtcp5OwgKLGjmDw2CNSm', // JuanCarlos1234
      'rol_id' => '1'
    )
  );


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      foreach ($this->usuarios as $usuario)
       {
          $user = new User();
          $user->name = $usuario['name'];
          $user->email = $usuario['email'];
          $user->password = $usuario['password'];
          $user->rol_id = $usuario['rol_id'];
          $user->save();
       }

      $this->command->info('Tabla users inicializada con datos');     
    }
}
