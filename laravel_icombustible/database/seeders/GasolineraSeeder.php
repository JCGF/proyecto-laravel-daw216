<?php

namespace Database\Seeders;

use App\Models\Gasolinera;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GasolineraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $url_api = "https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/";
      $data = json_decode( file_get_contents($url_api), true );

      DB::beginTransaction();

      foreach ($data['ListaEESSPrecio'] as $gasolinera)
       {
          $g = new Gasolinera();
          $g->id = $gasolinera['IDEESS'];
          $g->rotulo = Str::of($gasolinera['Rótulo'])->trim();
          $g->slug = Str::slug($g->rotulo .$g->id);
          $g->latitud = $gasolinera['Latitud'];
          $g->latitud = Str::of($g->latitud)->replace(',', '.');
          $g->longitud = $gasolinera['Longitud (WGS84)'];
          $g->longitud = Str::of($g->longitud)->replace(',', '.');
          $g->margen = $gasolinera['Margen'];
          $g->tipo_venta = $gasolinera['Tipo Venta'];
          $g->direccion = $gasolinera['Dirección'];
          $g->codigo_postal = $gasolinera['C.P.'];
          $g->localidad = $gasolinera['Localidad'];
          $g->municipio = $gasolinera['Municipio'];
          $g->provincia = $gasolinera['Provincia'];
          $g->save();
       }

      DB::commit();

      $this->command->info('Tabla gasolineras inicializada con datos.');   
    }
}
