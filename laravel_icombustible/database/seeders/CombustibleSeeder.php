<?php

namespace Database\Seeders;

use App\Models\Combustible;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CombustibleSeeder extends Seeder
{

    private $combustibles = array(
      array(
        'nombre' => 'Gasolina 95 E5',
        'nombre_comercial' => 'Sin Plomo 95',
        'etiqueta_europea' => 'E5',
      ),
      array(
        'nombre' => 'Gasolina 95 E10',
        'nombre_comercial' => 'Sin Plomo 95',
        'etiqueta_europea' => 'E10',
      ),
      array(
        'nombre' => 'Gasolina 95 E5 Premium',
        'nombre_comercial' => 'Sin Plomo 95+',
        'etiqueta_europea' => 'E5',
      ),
      array(
        'nombre' => 'Gasolina 98 E5',
        'nombre_comercial' => 'Sin Plomo 98',
        'etiqueta_europea' => 'E5',
      ),
      array(
        'nombre' => 'Gasolina 98 E10',
        'nombre_comercial' => 'Sin Plomo 98',
        'etiqueta_europea' => 'E10',
      ),
      array(
        'nombre' => 'Gasóleo A',
        'nombre_comercial' => 'Gasóleo A',
        'etiqueta_europea' => '',
      ),
      array(
        'nombre' => 'Gasóleo Premium',
        'nombre_comercial' => 'Gasóleo A+',
        'etiqueta_europea' => '',
      ),
      array(
        'nombre' => 'Gasóleo B',
        'nombre_comercial' => 'Gasóleo B',
        'etiqueta_europea' => '',
      ),
      array(
        'nombre' => 'Bioetanol',
        'nombre_comercial' => 'Bioetanol',
        'etiqueta_europea' => '',
      ),
      array(
        'nombre' => 'Biodiésel',
        'nombre_comercial' => 'Biodiésel',
        'etiqueta_europea' => '',
      ),
      array(
        'nombre' => 'Gases licuados del petróleo (GLP)',
        'nombre_comercial' => 'Gases licuados del petróleo',
        'etiqueta_europea' => 'CNG',
      ),
      array(
        'nombre' => 'Gas natural comprimido (GNC)',
        'nombre_comercial' => 'Gas natural comprimido',
        'etiqueta_europea' => 'CNG',
      ),
      array(
        'nombre' => 'Gas natural licuado (GNL)',
        'nombre_comercial' => 'Gas natural licuado',
        'etiqueta_europea' => 'LNG',
      )

    ); 

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      foreach ($this->combustibles as $combustible)
        {
          $c = new Combustible();
					$c->nombre = $combustible['nombre'];
          $c->nombre_comercial = $combustible['nombre_comercial'];
          $c->etiqueta_europea = $combustible['etiqueta_europea'] ? $combustible['etiqueta_europea'] : NULL;
					$c->slug = Str::slug($combustible['nombre']);
          $c->save();
        }

        $this->command->info('Tabla combustibles inicializada con datos.');    
    }
}
