<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(RolSeeder::class);
      $this->call(UserSeeder::class);
      $this->call(CombustibleSeeder::class);
      $this->call(GasolineraSeeder::class);
      $this->call(HorarioSeeder::class);
      $this->call(PrecioSeeder::class);
      $this->call(CombustibleGasolineraSeeder::class);
      $this->command->info('Seeders inicializados con éxito.');  
    }
}
