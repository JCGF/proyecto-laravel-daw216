<?php

namespace Database\Seeders;

use App\Models\Combustible;
use App\Models\Precio;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PrecioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $url_api = "https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/";
      $data = json_decode( file_get_contents($url_api), true );

      DB::beginTransaction();

      foreach ($data['ListaEESSPrecio'] as $gasolinera)
       {

         if( $gasolinera['Precio Gasolina 95 E5'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gasolina 95 E5'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gasolina 95 E5')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }
        
         if( $gasolinera['Precio Gasolina 95 E10'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gasolina 95 E10'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gasolina 95 E10')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }

         if( $gasolinera['Precio Gasolina 95 E5 Premium'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gasolina 95 E5 Premium'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gasolina 95 E5 Premium')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }

         if( $gasolinera['Precio Gasolina 98 E5'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gasolina 98 E5'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gasolina 98 E5')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }

         if( $gasolinera['Precio Gasolina 98 E10'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gasolina 98 E10'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gasolina 98 E10')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }

         if( $gasolinera['Precio Gasoleo A'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gasoleo A'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gasóleo A')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }

         if( $gasolinera['Precio Gasoleo Premium'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gasoleo Premium'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gasóleo Premium')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }          

         if( $gasolinera['Precio Gasoleo B'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gasoleo B'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gasóleo B')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }
        
         if( $gasolinera['Precio Bioetanol'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Bioetanol'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Bioetanol')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }
         
         if( $gasolinera['Precio Biodiesel'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Biodiesel'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Biodiésel')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }

         if( $gasolinera['Precio Gases licuados del petróleo'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gases licuados del petróleo'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gases licuados del petróleo (GLP)')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }

         if( $gasolinera['Precio Gas Natural Comprimido'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gas Natural Comprimido'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gas natural comprimido (GNC)')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }

        if( $gasolinera['Precio Gas Natural Licuado'] != "")
          {
            $p = new Precio();
            $p->importe = $gasolinera['Precio Gas Natural Licuado'];
            $p->importe = Str::of($p->importe)->replace(',', '.');
            $p->combustible_id = Combustible::where('nombre', 'Gas natural licuado (GNL)')->first()->id;
            $p->gasolinera_id = $gasolinera['IDEESS'];
            $p->save();
          }

       }

      DB::commit();

      $this->command->info('Tabla precios inicializada con datos.'); 
    }
}
