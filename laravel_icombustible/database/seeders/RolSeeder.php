<?php

namespace Database\Seeders;

use App\Models\Rol;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class RolSeeder extends Seeder
{

    private $roles = array(
      array(
        'nombre' => 'Usuario', 
      ),
      array(
        'nombre' => 'Moderador',
      ),
      array(
        'nombre' => 'Administrador',
      )
    );


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      foreach ($this->roles as $rol)
        {
          $r = new Rol();
          $r->nombre = $rol["nombre"];
          $r->slug = Str::slug($rol["nombre"]);
          $r->save();
        }

        $this->command->info('Tabla roles inicializada con datos.');  
    }
}
