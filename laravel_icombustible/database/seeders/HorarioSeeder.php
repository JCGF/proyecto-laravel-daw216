<?php

namespace Database\Seeders;

use App\Models\Horario;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HorarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $url_api = "https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/";
      $data = json_decode( file_get_contents($url_api), true );

      DB::beginTransaction();

      foreach ($data['ListaEESSPrecio'] as $gasolinera)
       {

          $array_h = explode(';', $gasolinera['Horario']);
          foreach ($array_h as $horarios)
           {
              $h = new Horario();
              $h->gasolinera_id = $gasolinera['IDEESS'];
              $h->horario_apertura = Str::of($horarios)->trim();      
              $h->save();
           }
          
       }

      DB::commit();

      $this->command->info('Tabla horarios inicializada con datos.'); 
    }
}
