@extends('layouts.master')

@section('titulo')
Gasolinera {{$gasolinera->rotulo}}
@endsection

@section('leaflet')

  <!-- LEAFLET -->
  <link rel="stylesheet" href="{{ asset('/assets/leaflet/leaflet.css') }}">
  <script src="{{ asset('/assets/leaflet/leaflet.js') }}"></script>

  <!-- LEAFLET FULLSCREEN -->
  <link rel="stylesheet" href="{{ asset('/assets/leaflet.fullscreen-2.4.0/Control.FullScreen.css') }}" />
  <script src="{{ asset('/assets/leaflet.fullscreen-2.4.0/Control.FullScreen.js') }}"></script>

  <!-- LEAFLET MARKERCLUSTER -->
  <link rel="stylesheet" href="{{ asset('/assets/leaflet.markercluster-1.4.1/MarkerCluster.css') }}">
  <link rel="stylesheet" href="{{ asset('/assets/leaflet.markercluster-1.4.1/MarkerCluster.Default.css') }}">
  <script src="{{ asset('/assets/leaflet.markercluster-1.4.1/leaflet.markercluster.js') }}"></script>

@endsection

@section('custom')
  <script src="{{ url('/assets/js/gasolineras_show.js') }}"></script>
@endsection

@section('contenido')

  @php
    use App\Http\Controllers\FavoritoController;
    use App\Http\Controllers\PuntuacionController;
  @endphp

  <div class="row">
     <div class="col-sm-8 mx-auto">

        <!-- Titulo Gasolinera -->
        <div class="iw-title mb-4">
          <h3>Gasolinera {{$gasolinera->rotulo}} en {{$gasolinera->localidad}} ({{$gasolinera->provincia}}) </h3>
          <h6>{{$gasolinera->direccion}}</h6>
          <hr>
 
          <!-- Favorito -->
          @if ( Auth::check() )

            @php    
              $esFavorito = DB::table('favoritos')->where('user_id', Auth::user()->id)->where('gasolinera_id', $gasolinera->id)->get();
            @endphp
            
            <!-- Datos Ocultos -->
            <input id="userId" type="hidden" value="{{Auth::user()->id}}">
            <input id="gasolineraId" type="hidden" value="{{$gasolinera->id}}">

            @if ( $esFavorito->count() == 0 )

              <span class="fs-6" id="textoFavorito">Añadir a favoritos:<span>
                <span class="ms-2" id="favoritoCorazon" data-corazon="vacio">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                  </svg>
              </span>
              <br>
            @else

              <span class="fs-6" id="textoFavorito">Eliminar de favoritos:<span>
                <span class="ms-2" id="favoritoCorazon" data-corazon="lleno"> 
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-heart-fill" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                  </svg>
              </span>
              <br>
            @endif

          @endif

          <!-- Puntuaciones -->
          @php
            $votos = DB::table('puntuaciones')->where('gasolinera_id', $gasolinera->id)->get();
            $mediaP = DB::table('puntuaciones')->where('gasolinera_id', $gasolinera->id)->avg('valor');
          @endphp

          @if ( count($votos) == 0 )
            <span class="fs-6" id="textoVotos">
              Puntuación: 0 / 5  - ( 0 votos )
            <span>  
          @else
            <span class="fs-6" id="textoVotos">
              Puntuación: {{round($mediaP, 1)}} / 5  - ( {{count($votos)}} votos )
            <span>   
          @endif

          @if ( Auth::check() )

            @php
              $votoU = DB::table('puntuaciones')->where('gasolinera_id', $gasolinera->id)->where('user_id', Auth::user()->id)->get();
            @endphp

            <br>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1">
              <label class="form-check-label" for="inlineRadio1">1</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2">
              <label class="form-check-label" for="inlineRadio2">2</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3">
              <label class="form-check-label" for="inlineRadio3">3</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="4">
              <label class="form-check-label" for="inlineRadio4">4</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="5">
              <label class="form-check-label" for="inlineRadio5">5</label>
            </div>

            @if ( count($votoU) > 0 )

              <button type="button" id="botonVotar" data-existev="si" class="btn btn-success btn-sm">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                  <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                </svg>
                Votar
              </button>

              @php
                $nombreB = "#inlineRadio" .$votoU[0]->valor;
              @endphp
              <script type="text/javascript">
                $("<?= $nombreB ?>").prop("checked", true);
              </script>

            @else

              <button type="button" id="botonVotar" data-existev="no" class="btn btn-success btn-sm">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                  <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                </svg>
                Votar
              </button>

            @endif

          @endif
                     
        </div>
        
        <!-- Mapa Gasolinera -->
        <div id="mapid" class="w-100" style="height: 450px"></div>

        <!-- Datos y Precios de la gasolinera -->
        <div class="row mt-4 mb-4">
          <div class="col-sm-6 p-3 rounded" style="background-color: #ffffff">
            <h5>Gasolinera {{$gasolinera->rotulo}}</h5>
            <hr>
            
            @if ( count($gasolinera->horarios) > 1 )
              <h6><em>Horarios:</em></h6>
            @else
              <h6><em>Horario:</em></h6>
            @endif   
        
            @foreach ($gasolinera->horarios as $horario)
              <p class="text-muted"> {{ $horario->horario_apertura }} </p>
            @endforeach

            <h6 clas><em>Margen:</em></h6>
            <p class="text-muted"> 
              @if ($gasolinera->margen != "N")
                @if ($gasolinera->margen == "D")
                  Derecho
                @else
                  Izquierdo 
                @endif 
              @else
                No Aplica
              @endif  
            </p>

            <h6 clas><em>Tipo de Venta:</em></h6>
            <p class="">
              @if ($gasolinera->tipo_venta == "P")
                <span style="font-weight: bolder; color: #00662c">Pública</span>
              @else
                <span style="font-weight: bolder; color: #ff0000">Restringida (Socios o Cooperativistas)</span>
              @endif
            </p>  

            <h6><em>Dirección:</em></h6>
            <p class="text-muted"> {{$gasolinera->direccion}}</p>
        
            <h6><em>Codigo postal:</em></h6>
            <p class="text-muted"> {{$gasolinera->codigo_postal}}</p>
        
            <h6><em>Localidad:</em></h6>
            <p class="text-muted"> {{$gasolinera->localidad}}</p>
        
            <h6><em>Municipio:</em></h6>
            <p class="text-muted"> {{$gasolinera->municipio}}</p>
        
            <h6><em>Provincia:</em></h6>
            <p class="text-muted"> {{$gasolinera->provincia}}</p>

            <h6><em>Coordenadas GPS:</em></h6>
            <p class="text-muted"> {{$gasolinera->latitud}}, {{$gasolinera->longitud}} (Lat/Long) </p>
            
          </div>
          
          <div class="col-sm-6">
            
            @php
              use App\Http\Controllers\GasolineraController;

              $c = 0;
              $arrayPrecios = GasolineraController::extraerPreciosGasolinera($gasolinera->id);
            @endphp
  
            <div class="p-3 rounded" style="background-color: #ffffff">
              <h6>Precios revisados el {{ date('j/m/Y') }}.</h6>

              @if ( Auth::check() )
                <!-- Botón Repostar -->
                <a href="{{ route('repostajes.create', ['id'=>$gasolinera->id]) }}" id="botonRepostar" class="ms-2 mt-2 btn btn-secondary btn-sm" role="button">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-fuel-pump-fill" viewBox="0 0 16 16">
                    <title>Repostar</title>
                    <path d="M1 2a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v8a2 2 0 0 1 2 2v.5a.5.5 0 0 0 1 0V8h-.5a.5.5 0 0 1-.5-.5V4.375a.5.5 0 0 1 .5-.5h1.495c-.011-.476-.053-.894-.201-1.222a.97.97 0 0 0-.394-.458c-.184-.11-.464-.195-.9-.195a.5.5 0 0 1 0-1c.564 0 1.034.11 1.412.336.383.228.634.551.794.907.295.655.294 1.465.294 2.081V7.5a.5.5 0 0 1-.5.5H15v4.5a1.5 1.5 0 0 1-3 0V12a1 1 0 0 0-1-1v4h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V2Zm2.5 0a.5.5 0 0 0-.5.5v5a.5.5 0 0 0 .5.5h5a.5.5 0 0 0 .5-.5v-5a.5.5 0 0 0-.5-.5h-5Z"/>
                  </svg>
                  Repostar
                </a>  
              @endif

              <hr>

              @foreach ($gasolinera->combustibles as $combustible)  

                <span><em>{{$combustible->nombre_comercial}}</span>  <br>
                <span class="badge rounded-pill bg-dark p-3">{{$arrayPrecios[$c]->importe}}</span>  <br>

                @php  $c++;  @endphp
                
              @endforeach

            </div>
          </div>

        </div>

        <!-- Comentarios de la gasolinera -->
        <div class="row mb-4">
          <div class="col-sm-12 p-3 rounded" style="background-color: #ffffff">
              
            <div class="row">

              @php
                $arrayComentarios = GasolineraController::extraerComentariosGasolinera($gasolinera->id);
              @endphp

              <!-- Arcodeon Comentarios -->
              <div class="accordion" id="acordeonComentarios">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="encabezado1">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <span class="badge rounded-pill text-bg-primary me-2"> <?= count($arrayComentarios) ?> </span>
                      Comentarios
                    </button>
                  </h2>
                  <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="encabezado1" data-bs-parent="#acordeonComentarios">
                    <div class="accordion-body">

                      <!-- Caja de Comentarios -->
                      @if ( Auth::check() )
                        <div class="col-sm-12">
                          <label for="textoComentario" class="form-label">Escriba su opinión:</label>
                          <textarea class="form-control" id="textoComentario" rows="4"></textarea>
                          <button type="button" id="publicarComentario" data-usuarioid="{{Auth::user()->id}}" data-gasolineraid="{{$gasolinera->id}}" class="btn btn-primary mt-3">Publicar comentario</button>
                        </div>
                      @else
                        <div class="alert alert-warning m-0" role="alert">
                          Deberás iniciar sesión o registrarte para poder escribir tu opinón.
                        </div>
                      @endif

                      <!-- Listado de comentarios -->
                      <hr>
                      <div class="mt-3">
                        @foreach ($arrayComentarios as $comentario)
                          @php
                            $usuario = DB::table('users')->find($comentario->user_id);                       

                            if ($usuario->profile_photo_path != null) {
                              $img_usuario = "/storage/" .$usuario->profile_photo_path;
                            } else {
                              $img_usuario = "https://ui-avatars.com/api/?name=" .urlencode($usuario->name) ."&color=7F9CF5&background=EBF4FF&rounded=true&size=128";
                            }
                          @endphp

                          <img src="{{$img_usuario}}" alt="" width="42" height="40" class="rounded-circle">
                          <span class="fs-5 ms-2"> {{$usuario->name}} </span> <br>
                          <p id="pComentario{{$comentario->id}}" class="mt-2"> {{$comentario->texto}} </p>
                          <textarea class="form-control mt-2 mb-2" id="cajaComentario{{$comentario->id}}" rows="4" style="display: none">{{$comentario->texto}}</textarea>
                          
                          {{-- Comprobar si se ha mofificado el comentario --}}
                          @if ($comentario->fecha_publicacion != $comentario->updated_at)
                            <strong>*</strong> <small>{{$comentario->updated_at}}</small>  
                          @else
                            <small>{{$comentario->fecha_publicacion}}</small> 
                          @endif                        
                          
                          {{-- Comprobar si está registrado antes de imprimir los botones --}}
                          @if ( Auth::check() )
                          
                            {{-- Comprobar si es un usuario básico --}}
                            @if ( Auth::user()->rol_id == 1 )

                              {{-- Comprobar si es su propio comentario --}}
                              @if ( Auth::user()->id == $comentario->user_id )

                                <!-- Botón Editar Comentario -->
                                <button type="button" id="botonEditarC{{$comentario->id}}" data-comentarioid="{{$comentario->id}}" class="ms-2 btn btn-warning btn-sm botonEditarC">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                    <title>Editar comentario</title>
                                    <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                  </svg>
                                </button>
    
                                <!-- Botón Actualizar Comentario -->
                                <button type="button" id="botonActualizarC{{$comentario->id}}" data-comentarioid="{{$comentario->id}}" class="ms-2 btn btn-success btn-sm botonActualizarC" style="display: none">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-send-fill" viewBox="0 0 16 16">
                                    <title>Actualizar comentario</title>
                                    <path d="M15.964.686a.5.5 0 0 0-.65-.65L.767 5.855H.766l-.452.18a.5.5 0 0 0-.082.887l.41.26.001.002 4.995 3.178 3.178 4.995.002.002.26.41a.5.5 0 0 0 .886-.083l6-15Zm-1.833 1.89L6.637 10.07l-.215-.338a.5.5 0 0 0-.154-.154l-.338-.215 7.494-7.494 1.178-.471-.47 1.178Z"/>
                                  </svg>
                                </button>

                                <!-- Botón Borrar Comentario -->
                                <button type="button" data-comentarioid="{{$comentario->id}}" class="ms-1 btn btn-danger btn-sm botonBorrarC">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                    <title>Borrar comentario</title>
                                    <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
                                  </svg>
                                </button>

                              @endif   

                            @else                      
                              {{--Comprobar si es moderador o administrador --}}
                              @if ( Auth::user()->rol_id == 2 || Auth::user()->rol_id == 3 )
                                
                                {{-- Comprobar si es su propio comentario --}}
                                @if ( Auth::user()->id == $comentario->user_id )

                                  <!-- Botón Editar Comentario -->
                                  <button type="button" id="botonEditarC{{$comentario->id}}" data-comentarioid="{{$comentario->id}}" class="ms-2 btn btn-warning btn-sm botonEditarC">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                      <title>Editar comentario</title>
                                      <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                    </svg>
                                  </button>
      
                                  <!-- Botón Actualizar Comentario -->
                                  <button type="button" id="botonActualizarC{{$comentario->id}}" data-comentarioid="{{$comentario->id}}" class="ms-2 btn btn-success btn-sm botonActualizarC" style="display: none">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-send-fill" viewBox="0 0 16 16">
                                      <title>Actualizar comentario</title>
                                      <path d="M15.964.686a.5.5 0 0 0-.65-.65L.767 5.855H.766l-.452.18a.5.5 0 0 0-.082.887l.41.26.001.002 4.995 3.178 3.178 4.995.002.002.26.41a.5.5 0 0 0 .886-.083l6-15Zm-1.833 1.89L6.637 10.07l-.215-.338a.5.5 0 0 0-.154-.154l-.338-.215 7.494-7.494 1.178-.471-.47 1.178Z"/>
                                    </svg>
                                  </button>

                                  <!-- Botón Borrar Comentario -->
                                  <button type="button" data-comentarioid="{{$comentario->id}}" class="ms-1 btn btn-danger btn-sm botonBorrarC">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                      <title>Borrar comentario</title>
                                      <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
                                    </svg>
                                  </button>
                                
                                @else

                                  <!-- Botón Borrar Comentario -->
                                  <button type="button" data-comentarioid="{{$comentario->id}}" class="ms-1 btn btn-danger btn-sm botonBorrarC">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                      <title>Borrar comentario</title>
                                      <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
                                    </svg>
                                  </button>  

                                @endif                                 

                              @endif

                            @endif

                          @endif

                          <hr>
                        @endforeach                
                      </div>

                    </div>
                  </div>
                </div>
              </div>

          </div>
        </div>

    </div>
  </div>


  <script>

    // Variable gasolinera
    let gasolinera = @json($gasolinera);

    // Layers
    var osmCarto = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 19
    });

    var mapbox = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGotd2luc3RvbiIsImEiOiJja2k0Z2hhamg0M2lvMnNrenFwemZyNmx1In0.BNJsMPaIBO5fcFYv7prXAQ', {
      maxZoom: 19,
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1
    });

    var PNOA_Ortoimagen = L.tileLayer.wms('https://www.ign.es/wms-inspire/pnoa-ma', {
      layers: 'OI.OrthoimageCoverage',
      format: 'image/png',
      transparent: false,
      attribution: 'PNOA cedido por © <a href="https://www.ign.es/web/ign/portal">Instituto Geográfico Nacional de España</a>',
      maxZoom: 19
    });  

    // Group of layers
    var baseLayers = 
    {
        "PNOA Imagen": PNOA_Ortoimagen,	
        "OSM Carto": osmCarto,
        "Mapbox": mapbox, 	  
    };

    // Map
    var mymap = L.map('mapid', {
      center: [gasolinera["latitud"], gasolinera["longitud"]],
      zoom: 17,
      zoomControl: false,
      layers: [PNOA_Ortoimagen, osmCarto, mapbox]
    });

    // Add map layer to the Map
    L.control.layers(baseLayers, null, {shortLayers: false}).addTo(mymap);

    // Add Zoom Controls
    L.control.zoom({
      zoomInTitle: 'Acercar el Zoom',
      zoomOutTitle: 'Alejar el Zoom'
    }).addTo(mymap);

    // Fullscreen Control
    L.control.fullscreen({
      position: 'topleft', // change the position of the button can be topleft, topright, bottomright or bottomleft, default topleft
      title: 'Pantalla Completa', // change the title of the button, default Full Screen
      titleCancel: 'Salir del modo pantalla completa', // change the title of the button when fullscreen is on, default Exit Full Screen
      content: null, // change the content of the button, can be HTML, default null
      forceSeparateButton: true, // force separate button to detach from zoom buttons, default false
      forcePseudoFullscreen: true, // force use of pseudo full screen even if full screen API is available, default false
      fullscreenElement: false // Dom element to render in full screen, false by default, fallback to map._container
    }).addTo(mymap);

    // Icons
    var genericoIcon = L.icon({
      iconUrl: '/assets/imgs/generico.jpg',
      iconSize: [35, 35],
    });

    // Variable nuevoM
    let nuevoM = L.marker([ gasolinera["latitud"], gasolinera["longitud"] ], {icon: genericoIcon});

    // Add marker to Layer
    mymap.addLayer( nuevoM );

  </script>

@endsection