@extends('layouts.master')

@section('titulo')
Gasolineras
@endsection

@section('contenido')


<div class="container pt-4">
  <div class="row">

    @foreach ($gasolineras as $gasolinera)
      <div class="mb-3 col-sm-6 col-md-4 col-lg-3">
        <div class="card-columns-fluid">
          <div class="card bg-light">    
            
            <div class="card-body">

              <h5 class="card-title">{{ $gasolinera->rotulo }}</h5>

              <span class="card-text cursiva">Margen:
                @if ($gasolinera->margen != "N")
                  @if ($gasolinera->margen == "D")
                    Derecho
                  @else
                    Izquierdo 
                  @endif 
                @else
                  No Aplica
                @endif    
              </span> <br> 
 
              <span class="card-text cursiva">Municipio: {{ $gasolinera->municipio }}</span> <br>
              <span class="card-text cursiva">CP: {{ $gasolinera->codigo_postal }}</span> <br>
              
              @php
                $url_localizacion = 'https://www.google.com/maps/search/?api=1&query=' .$gasolinera->latitud .',' .$gasolinera->longitud;  
              @endphp
              <span class="card-text"> <a href="{{$url_localizacion}}" target="_blank">Localización</a></span> <br>
              
              <a href="{{route('gasolineras.show', $gasolinera)}}" class="btn btn-primary mt-2">Datos de la gasolinera</a>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    {{ $gasolineras->links() }}

  </div>
</div>

@endsection