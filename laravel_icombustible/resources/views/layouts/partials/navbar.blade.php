<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container-fluid">
    <a href="{{ route('home') }}" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
      <img class="bi me-2" width="44" height="42" role="img" src="{{ asset('/assets/imgs/iCombustible-logotipo.svg') }}" aria-label="iCombustible">
      <a class="navbar-brand" href="{{ route('home') }}">iCombustible</a>
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
        <li><a href="{{ route('gasolineras.index') }}" class="nav-link {{ request()->routeIs('gasolineras.*') && !request()->routeIs('gasolineras.show')? 'text-white' : 'text-secondary'}} px-2">
        Listado de Gasolineras</a></li>
        <li><a href="{{ route('combustibles.index') }}" class="nav-link {{ request()->routeIs('combustibles.*') && !request()->routeIs('inicio.index')? 'text-white' : 'text-secondary'}} px-2">
        Listado de Combustibles</a></li>
        <li><a href="{{ route('calculadoras.index') }}"  class="nav-link {{ request()->routeIs('calculadoras.*') && !request()->routeIs('inicio.index')? 'text-white' : 'text-secondary'}} px-2">
        Calculadora</a></li>
      </ul>

      {{--
      <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
        <input type="search" class="form-control form-control-dark" placeholder="Search..." aria-label="Search">
      </form>
      --}}

      @if (!Auth::check())
        <div class="text-end">
          <a href="{{ route('login') }}" class="btn btn-outline-light me-2" role="button">Identifícate</a>
          <a href="{{ route('register') }}" class="btn btn-success me-2"  role="button">Regístrate</a>
        </div>
      @else
        <div class="dropdown text-end">
          @php
            if (Auth::user()->profile_photo_path != null) {
              $img_usuario = "/storage/" .Auth::user()->profile_photo_path;
            } else {
              $img_usuario = "https://ui-avatars.com/api/?name=" .urlencode(Auth::user()->name) ."&color=7F9CF5&background=EBF4FF&rounded=true&size=128";
            }
          @endphp

          <a href="#" class="d-block link-light text-decoration-none dropdown-toggle" id="dropdownUsuario" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="{{$img_usuario}}" alt="" width="42" height="40" class="rounded-circle">
          </a>
          <ul class="dropdown-menu dropdown-menu-end text-small mt-2" aria-labelledby="dropdownUsuario">
            <li><a class="dropdown-item" href="{{ route('profile.show') }}">Perfil</a></li>
            <li><a class="dropdown-item" href="{{ route('dashboard') }}">Panel de usuario</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Finalizar sesión</a>
              <form id="logout-form" action="{{ route('logout')}} " method="POST" style="display: none;">
                @csrf
              </form>
            </li>
          </ul>
        </div>
      @endif

    </div>
  </div>
</nav>
