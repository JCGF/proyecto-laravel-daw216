<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="{{ asset('/assets/imgs/iCombustible-logotipo.svg') }}">
    <meta name="description" content="Página web comparativa de combustibles.">
    <meta name="keywords" content="Gasolineras, iCombustible, Combustibles">
    <meta name="author" content="Juan Carlos González Fernández">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('titulo')</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('/assets/bootstrap-5.3.0/css/bootstrap.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('/assets/css/estilo.css') }}">
    <!--  Bootstrap JavaScript -->
    <script src="{{ asset('/assets/bootstrap-5.3.0/js/bootstrap.bundle.min.js') }}"></script>
    <!-- jQuery -->
    <script src="{{ asset('/assets/jquery/jquery-3.7.0.min.js') }}"></script>

    {{-- Leaflet --}}
    @yield('leaflet')
    {{-- Custom --}}
    @yield('custom')

  </head>
  <body>
    
    @include('layouts.partials.navbar')

    <div class="container-fluid">
      @yield('contenido')
    </div>

  </body>
</html>