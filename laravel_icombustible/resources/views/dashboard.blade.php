<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-6">
                {{-- <x-jet-welcome /> --}}
                <h1 class="text-5xl font-extrabold dark:text-white">iCombustible
                  <small class="ml-2 font-semibold text-gray-500 dark:text-gray-400">Listado de herramientas:</small>
                </h1>
                <hr class="mt-4 mb-4">
                <a href="{{ route('calculadoras.index') }}">
                  <button type="button" class="text-white bg-yellow-400 hover:bg-yellow-500 focus:outline-none focus:ring-4 focus:ring-yellow-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:focus:ring-yellow-900">Calculadora</button>
                </a>
                <br>
                <a href="{{ route('favoritos.index') }}">
                  <button type="button" class="text-white bg-green-700 hover:bg-green-800 focus:outline-none focus:ring-4 focus:ring-green-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Listado de favoritos</button>
                </a>
                <br>
                <a href="{{ route('repostajes.index') }}">
                  <button type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Historial de Repostajes</button>
                </a>
            </div>
        </div>
    </div>
</x-app-layout>
