@extends('layouts.master')

@section('titulo')
iCombustible
@endsection

@section('leaflet')

  <!-- LEAFLET -->
  <link rel="stylesheet" href="{{ asset('/assets/leaflet/leaflet.css') }}">
  <script src="{{ asset('/assets/leaflet/leaflet.js') }}"></script>

  <!-- LEAFLET FULLSCREEN -->
  <link rel="stylesheet" href="{{ asset('/assets/leaflet.fullscreen-2.4.0/Control.FullScreen.css') }}" />
  <script src="{{ asset('/assets/leaflet.fullscreen-2.4.0/Control.FullScreen.js') }}"></script>

  <!-- LEAFLET MARKERCLUSTER -->
  <link rel="stylesheet" href="{{ asset('/assets/leaflet.markercluster-1.4.1/MarkerCluster.css') }}">
  <link rel="stylesheet" href="{{ asset('/assets/leaflet.markercluster-1.4.1/MarkerCluster.Default.css') }}">
  <script src="{{ asset('/assets/leaflet.markercluster-1.4.1/leaflet.markercluster.js') }}"></script>

@endsection


@section('contenido')

  @php
    use App\Http\Controllers\InicioController;
    
    $arrayGasolineras = InicioController::extraerGasolineras();
    $arrayHorariosGasolineras = InicioController::extraerHorariosGasolineras();

  @endphp 

  <div class="row">
    <div class="col-sm-10 mx-auto">

      <!-- Banner de Gasolineras -->
      <div class="iw-title mt-2 mb-4">
        <h5> En España existen {{ number_format(count($arrayGasolineras), 0, ',', '.') }} gasolineras repartidas por toda su geografía. <br> Con nuestro buscador podrás ver las diferentes gasolineras que existen en cada provincia o municipio de España y conocer sus precios.</h5>
      </div>

      <!-- Mapa Gasolinera -->
      <div id="mapid" class="w-100" style="height: 600px"></div>

  </div>

  <script>

    // Variables
    let marcadores = @json($arrayGasolineras);
    let horarios = @json($arrayHorariosGasolineras);

    // Layers
    var osmCarto = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 19
    });

    var mapbox = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGotd2luc3RvbiIsImEiOiJja2k0Z2hhamg0M2lvMnNrenFwemZyNmx1In0.BNJsMPaIBO5fcFYv7prXAQ', {
      maxZoom: 19,
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1
    });

    var PNOA_Ortoimagen = L.tileLayer.wms('https://www.ign.es/wms-inspire/pnoa-ma', { 
      layers: 'OI.OrthoimageCoverage',
      format: 'image/png',
      transparent: false,
      attribution: 'PNOA cedido por © <a href="https://www.ign.es/web/ign/portal">Instituto Geográfico Nacional de España</a>',
      maxZoom: 19
    });  

    // Group of layers
    var baseLayers = 
    {
        "PNOA Imagen": PNOA_Ortoimagen,	
        "OSM Carto": osmCarto,
        "Mapbox": mapbox, 	  
    };

    // Map
    var mymap = L.map('mapid', {
      center: [39.550, -3.950],
      zoom: 6,
      zoomControl: false,
      layers: [PNOA_Ortoimagen, osmCarto, mapbox]
    });

    // Add map layer to the Map
    L.control.layers(baseLayers, null, {shortLayers: false}).addTo(mymap);

    // Add Zoom Controls
    L.control.zoom({
      zoomInTitle: 'Acercar el Zoom',
      zoomOutTitle: 'Alejar el Zoom'
    }).addTo(mymap);

    // Fullscreen Control
    L.control.fullscreen({
      position: 'topleft', // change the position of the button can be topleft, topright, bottomright or bottomleft, default topleft
      title: 'Pantalla Completa', // change the title of the button, default Full Screen
      titleCancel: 'Salir del modo pantalla completa', // change the title of the button when fullscreen is on, default Exit Full Screen
      content: null, // change the content of the button, can be HTML, default null
      forceSeparateButton: true, // force separate button to detach from zoom buttons, default false
      forcePseudoFullscreen: true, // force use of pseudo full screen even if full screen API is available, default false
      fullscreenElement: false // Dom element to render in full screen, false by default, fallback to map._container
    }).addTo(mymap);

    // Icons
    var genericoIcon = L.icon({
      iconUrl: '/assets/imgs/generico.jpg',
      iconSize: [35, 35],
    });

    // Group of markers
    var grupoMarcadores= L.markerClusterGroup( {maxClusterRadius: 50});

    // Recorrer el array de marcadores
    for (let m in marcadores)
    {
        let html = `<div id="iw-container"> 
                      <div class="iw-title"> ${marcadores[m]["rotulo"]} </div>
                      <div class="iw-content">
                        <p> ${marcadores[m]["direccion"]} <br> 
                            ${marcadores[m]["localidad"]} (${marcadores[m]["provincia"]}) <br>
                            <b> - Horario: </b> <br>`;
          
        // Recorrer el array de horarios
        for (let h in horarios[marcadores[m]["id"]])
        {
          html += `${horarios[marcadores[m]["id"]][h]["horario_apertura"]} <br>`;
        }

        html += `</p> <p class="mt-2"> <a href="gasolineras/${marcadores[m]["slug"]}" class="btn btn-primary btn-sm blanco" role="button" target="_blank"> Ver ficha completa </a>`;
        html += `</p></div> </div>`;

        // Variable nuevoM
        let nuevoM;

        nuevoM = L.marker([ marcadores[m]["latitud"], marcadores[m]["longitud"] ], {icon: genericoIcon}).bindPopup(html);
        
        // Add marker to the group of markers
        grupoMarcadores.addLayer( nuevoM );
    }
    
    // Add group of markers to Layer
    mymap.addLayer( grupoMarcadores );
  
  </script>


@endsection