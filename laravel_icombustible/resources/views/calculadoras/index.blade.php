@extends('layouts.master')

@section('titulo')
Calculadora
@endsection

@section('custom')
  <script src="{{ url('/assets/js/calculadoras_index.js') }}"></script>
@endsection

@section('contenido')

<div class="container">
  <div class="row">
    <div class="col-sm-8 mx-auto">

      <!-- Banner de Calculadora -->
      <div class="iw-title mt-2 mb-4">
        <h5> Con esta herramienta podrás calcular el costo de tu repostaje.</h5>
      </div>

      <div class="col-sm-6 mx-auto p-4 rounded bg-white">
          <legend>Calculadora de repostaje:</legend>
          <div class="mb-3">
            <label for="cLitrosRepostados" class="form-label">Litros repostados:</label>
            <input type="number" min="1" step="0.1" class="form-control" id="cLitrosRepostados">
          </div>
          <div class="mb-3">
            <label for="cPrecioPorLitro" class="form-label">Precio por litro:</label>
            <input type="number" min="0" step="0.1" class="form-control" id="cPrecioPorLitro">
          </div>
          <div class="mb-3">
            <label for="cImporteTotal" class="form-label">Importe total:</label>
            <div class="input-group">
              <input type="text" disabled class="form-control" id="cImporteTotal">
              <div class="input-group-text">€</div>
            </div>
          </div>

          <button type="button" id="bCalcularI" class="btn btn-warning">Calcular Importe</button>
      </div>


    </div>
  </div>
</div>

@endsection