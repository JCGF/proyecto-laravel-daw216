@extends('layouts.master')

@section('titulo')
Combustibles
@endsection

@section('contenido')

@php
  use App\Http\Controllers\CombustibleController;
  use App\Http\Controllers\GasolineraController;

  $arrayPrecios = CombustibleController::mediaPrecioCombustibles();
  $numGasolineras = GasolineraController::numeroGasolineras();
  // Variable contador
  $c = 0;

@endphp 


<div class="container">
  <div class="row">
    <div class="col-sm-8 mx-auto">

      <!-- Banner de Precios Medios -->
      <div class="iw-title mt-2 mb-4">
        <h5> En España existen {{ number_format($numGasolineras, 0, ',', '.') }} gasolineras repartidas por toda su geografía. <br> Con esta tabla podrás conocer el precio medio de cada combustible.</h5>
      </div>

      <!-- Tabla Precios Medios -->
      <table class="table table-bordered table-striped bg-white">
        <thead>
          <tr>
            <th scope="col">Tipo de Combustible</th>
            <th scope="col">Precio medio HOY</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($combustibles as $combustible)
            <tr>
              <td> {{$combustible->nombre}} </td>
              <td> <span class="badge rounded-pill bg-dark p-3">{{ number_format($arrayPrecios[$c]->importe_medio, 3) }}</span> </td>
            </tr>
            @php $c++; @endphp
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>

@endsection