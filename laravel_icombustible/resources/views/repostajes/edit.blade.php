@extends('layouts.master')

@section('titulo')
Editar repostaje
@endsection

@section('contenido')

<div class="row">
  <div class="offset-md-3 col-md-6 pt-3 pb-3">
    <div class="card">
      <div class="card-header text-center">
        <h4>Editar un repostaje</h4>
      </div>

      @php
        $repostaje = DB::table('repostajes')->where('id', $id)->first();
      @endphp

      <div class="card-body" style="padding:30px">
        <form action="{{ route('repostajes.update')}}" method="POST" enctype="multipart/form-data">
          @csrf

          <input type="hidden" name="id" id="user_id" value="{{$repostaje->id}}">
          <input type="hidden" name="user_id" id="user_id" value="{{$repostaje->user_id}}">
          <input type="hidden" name="gasolinera_id" id="gasolinera_id" value="{{$repostaje->gasolinera_id}}">

          <label for="importe-url" class="form-label">Importe:</label>
          <div class="input-group pb-3">
            <input type="number" id="importe" name="importe" min="1" step="0.1" value="{{$repostaje->importe}}" class="form-control" required>
            <span class="input-group-text">€</span>
          </div>

          <div class="form-group pb-3">
            <label for="litros_repostados" class="pb-2">Litros repostados:</label>
            <input type="number" id="litros_repostados" name="litros_repostados" min="1" step="0.1" value="{{$repostaje->litros_repostados}}" class="form-control" required>
          </div>

          <div class="form-group pb-3">
            <label for="fecha_repostaje" class="pb-2 pe-4">Fecha del repostaje:</label>
            <input type="date" id="fecha_repostaje" name="fecha_repostaje" value="{{$repostaje->fecha_repostaje}}" required>
          </div>
         
          <div class="form-group text-center">
            <button type="submit" class="btn btn-success py-2 px-5 mt-3">
              Editar repostaje
            </button>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>

@endsection