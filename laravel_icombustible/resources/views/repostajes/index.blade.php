@extends('layouts.master')

@section('titulo')
Repostajes
@endsection

@section('contenido')


<div class="container pt-4">
  <div class="row">
    
    <h3>Listado de Repostajes:</h3>
    <hr>
    
    @if (count($repostajes) > 0)
      
      <table class="table table-striped">
        <thead class="table-light">
          <th>Rótulo</th>
          <th>Importe</th>
          <th>Litros repostados</th>
          <th>Fecha del repostaje</th>
          <th>Editar</th>
        </thead>
        <tbody>
          @foreach ($repostajes as $r)

            @php
              $gasolinera = DB::table('gasolineras')->where('id', $r->gasolinera_id)->first();
            @endphp

            <tr>
              <td><a href="http://icombustible.es/gasolineras/{{$gasolinera->slug}}" target="_blank">{{$gasolinera->rotulo}}<a></td>
              <td> {{$r->importe}} €</td>
              <td> {{$r->litros_repostados}} l </td>
              <td> {{$r->fecha_repostaje}} </td>
              <td>
                <!-- Botón Editar Repostaje -->
                <a href="{{route('repostajes.edit', ['id'=>$r->id])}}" id="botonEditarR" class="ms-2 btn btn-warning btn-sm botonEditarR" role="button">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                    <title>Editar repostaje</title>
                    <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                  </svg>
                </a>
              </td>
            </tr>  
          @endforeach
        </tbody>

      </table>
    @else

      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
          <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
        </symbol>
      </svg>
      <div class="alert alert-primary d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
        <div>
          No dispones de ningún repostaje realizado.
        </div>
      </div>

    @endif

  </div>
</div>

@endsection