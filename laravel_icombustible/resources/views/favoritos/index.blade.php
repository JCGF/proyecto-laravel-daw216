@extends('layouts.master')

@section('titulo')
Favoritos
@endsection

@section('contenido')


<div class="container pt-4">
  <div class="row">
    
    <h3>Listado de favoritos:</h3>
    <hr>

    @if (count($gasolinerasF) > 0)
      
      <table class="table table-striped">
        <thead class="table-light">
          <th>Rótulo</th>
          <th>Dirección</th>
          <th>Localidad</th>
          <th>GPS</th>
          
        </thead>
        <tbody>
          @foreach ($gasolinerasF as $gasolinera)

            @php
              $url_localizacion = 'https://www.google.com/maps/search/?api=1&query=' .$gasolinera->latitud .',' .$gasolinera->longitud;  
            @endphp
            <tr>
              <td><a href="http://icombustible.es/gasolineras/{{$gasolinera->slug}}" target="_blank">{{$gasolinera->rotulo}}<a></td>
              <td>{{$gasolinera->direccion}}</td>
              <td>{{$gasolinera->localidad}} ({{$gasolinera->codigo_postal}})</td>
              <td>
                <a href="{{$url_localizacion}}" target="_blank">
                  <button type="button" class="btn btn-primary">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                      <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"></path>
                    </svg>
                  </button>
                </a>
              </td>
            </tr>  
          @endforeach
        </tbody>

      </table>
    
    @else

      <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
          <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
        </symbol>
      </svg>
      <div class="alert alert-primary d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
        <div>
          No dispones de ninguna gasolinera guardada como favorita.
        </div>
      </div>

    @endif

  </div>
</div>

@endsection