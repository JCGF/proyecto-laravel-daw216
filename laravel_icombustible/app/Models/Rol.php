<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
  use HasFactory;

  public $table = 'roles';
  protected $guarded = [];

  
  public function getRouteKeyName()
   {
      return 'slug';
   } 

  // Relación uno a uno (User-Rol) (INVERSA) 
  public function user()
   {
      return $this->belongsTo(User::class);
   }

}
