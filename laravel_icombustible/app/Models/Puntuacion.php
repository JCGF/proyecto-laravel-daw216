<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Puntuacion extends Model
{
  use HasFactory;

  public $table = 'puntuaciones';
  protected $guarded = [];


  // Relación uno a muchos (Gasolinera-Puntuacion) (INVERSA)
  public function gasolinera()
   {
			return $this->belongsTo(Gasolinera::class);
   }

  // Relación uno a muchos (User-Puntuacion) (INVERSA)
  public function user()
   {
			return $this->belongsTo(User::class);
   }
  
}
