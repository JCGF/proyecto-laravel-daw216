<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'rol_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    // Relación uno a uno (User-Rol)
    public function rol()
     {
        return $this->hasOne(Rol::class);
     }
  
    // Relación uno a uno (Comentario-User) (INVERSA)
    public function comentario()
     {
        return $this->belongsTo(Comentario::class);
     }
    
    // Relación uno a muchos (User-Puntuacion)
    public function puntuaciones()
     {
        return $this->hasMany(Puntuacion::class);
     }

  // Relación uno a muchos (User-Favorito)
  public function favoritos()
   {
      return $this->hasMany(Favorito::class);
   }
   
  // Relación uno a muchos (User-Repostaje)
  public function repostajes()
   {
      return $this->hasMany(Repostaje::class);
   }   
     
}
