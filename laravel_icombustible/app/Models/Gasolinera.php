<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gasolinera extends Model
{
  use HasFactory;

  public $table = 'gasolineras';
  protected $guarded = [];


  public function getRouteKeyName()
   {
      return 'slug';
   }

	// Relación muchos a muchos
	public function combustibles()
	 {
      return $this->belongsToMany(Combustible::class);
	 } 

  // Relación uno a muchos (Gasolinera-Horario)
  public function horarios()
   {
      return $this->hasMany(Horario::class);
   }  

  // Relación uno a uno (Precio-Gasolinera) (INVERSA)
  public function precio()
   {
      return $this->belongsTo(Precio::class);
   }

  // Relación uno a muchos (Gasolinera-Comentario)
  public function comentarios()
   {
      return $this->hasMany(Comentario::class);
   }

  // Relación uno a muchos (Gasolinera-Puntuacion)
  public function puntuaciones()
   {
      return $this->hasMany(Puntuacion::class);
   }
   
  // Relación uno a uno (Favorito-Gasolinera) (INVERSA)
  public function favorito()
   {
      return $this->belongsTo(Favorito::class);
   }
   
  // Relación uno a uno (Repostaje-Gasolinera) (INVERSA)
  public function repostaje()
   {
      return $this->belongsTo(Repostaje::class);
   }   

}
