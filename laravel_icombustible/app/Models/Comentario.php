<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
  use HasFactory;

  public $table = 'comentarios';
  protected $guarded = [];


  // Relación uno a muchos (Gasolinera-Comentario) (INVERSA)
  public function gasolinera()
   {
      return $this->belongsTo(Gasolinera::class);
   }

  // Relación uno a uno (Comentario-User) 
  public function user()
    {
      return $this->belongsTo(User::class);
    }
    
}
