<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repostaje extends Model
{
  use HasFactory;
  
  public $table = 'repostajes';
  protected $guarded = [];


  // Relación uno a uno (Repostaje-Gasolinera)
  public function gasolinera()
   {
      return $this->hasOne(Gasolinera::class);
   }  

  // Relación uno a muchos (User-Repostaje) (INVERSA)
  public function user()
   {
			return $this->belongsTo(User::class);
   }
     
}
