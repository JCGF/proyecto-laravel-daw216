<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Precio extends Model
{
  use HasFactory;

  public $table = 'precios';
  protected $guarded = [];

  // Relación uno a uno (Precio-Combustible)
  public function combustible()
   {
      return $this->hasOne(Combustible::class);
   }
 
  // Relación uno a uno (Precio-Gasolinera)
  public function gasolinera()
   {
      return $this->hasOne(Gasolinera::class);
   }   

}
