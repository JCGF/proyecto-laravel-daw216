<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
  use HasFactory;

  public $table = 'horarios';
  protected $guarded = [];
  
  // Relación uno a muchos (Gasolinera-Horario) (INVERSA)
  public function gasolinera()
   {
			return $this->belongsTo(Gasolinera::class);
   }

}
