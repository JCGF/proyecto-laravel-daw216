<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Combustible extends Model
{
  use HasFactory;

  public $table = 'combustibles';
  protected $guarded = [];  

 
  public function getRouteKeyName()
   {
      return 'slug';
   }   

	// Relación muchos a muchos
	public function gasolineras()
	 {
      return $this->belongsToMany(Gasolinera::class);
	 }    

  // Relación uno a uno (Precio-Combustible) (INVERSA)
  public function precio()
   {
      return $this->belongsTo(Precio::class);
   }

}
