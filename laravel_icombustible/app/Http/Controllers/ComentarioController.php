<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use Illuminate\Http\Request;

class ComentarioController extends Controller
{
  
  public static function insertarComentario(Request $request)
   { 
      // Crear la fecha de publicacion
      $fechaP = date('Y-m-d H:i:s');

      // Formar el array de datos
      $datos = $request->all();
      $datos['fecha_publicacion']	= $fechaP;

      // Crear y guardar el comentario
      $comentario = Comentario::create($datos);
      return $comentario->id;
   }

  public static function modificarComentario(Request $request)
   { 
      // Crear la fecha de la modificación
      $fechaM = date('Y-m-d H:i:s');  

      // Formar el array de datos
      $datos = $request->all();

      // Modificar el comentario
      $comentario = Comentario::where('id', $datos['comentario_id'])->first();
      $comentario->texto = $datos['texto'];
      $comentario->updated_at = $fechaM;
      $comentario->save();

      return $comentario->id;
   }

  public static function visibilidadComentario(Request $request)
   {
      // Formar el array de datos
      $datos = $request->all();

      // Modificar el comentario
      $comentario = Comentario::where('id', $datos['comentario_id'])->first();

      // Comprobar comentario es visible
      if ($comentario->visible == 1) {
        // Ocultar comentario
        $comentario->visible = 0;
      } else {
        // Mostrar comentario
        $comentario->visible = 1;
      }
      $comentario->save();

      return $comentario->id;
   }  

}
