<?php

namespace App\Http\Controllers;

use App\Models\Combustible;
use Illuminate\Support\Facades\DB;

class CombustibleController extends Controller
{
  public function index()
   {
      $combustibles = Combustible::all();

      return view('combustibles.index', compact('combustibles'));
   }

  public static function mediaPrecioCombustibles()
   {
      // Extraer: combustible_id y la media de los importes por tipo de combustible de la BBDD
      $precios = DB::select('SELECT combustible_id, AVG(importe) as importe_medio FROM precios GROUP BY combustible_id');

      return $precios;
   }

}
