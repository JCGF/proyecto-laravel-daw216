<?php

namespace App\Http\Controllers;

use App\Models\Favorito;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FavoritoController extends Controller
{

  public function index()
   {
      $favoritos = DB::table('favoritos')->where('user_id', Auth::user()->id)->get();
      $gasolinerasF = [];

      // Recorrer el listado de favoritos
      foreach ($favoritos as $f) {   
        $gasolinerasF[] = DB::table('gasolineras')->find($f->gasolinera_id);
      }

      return view('favoritos.index', compact('gasolinerasF'));
   }

  public static function insertarFavorito(Request $request)
   { 
      // Formar el array de datos
      $datos = $request->all();

      // Crear y guardar el favorito
      $favorito = Favorito::create($datos);
      return $favorito->id;
   }
   
  public static function borrarFavorito(Request $request)
   { 
      // Formar el array de datos
      $datos = $request->all();

      // Borrar el favorito
      $borrado = Favorito::where('user_id', $datos['user_id'])->where('gasolinera_id', $datos['gasolinera_id'])->delete();
   }
}
