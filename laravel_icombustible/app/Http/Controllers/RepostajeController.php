<?php

namespace App\Http\Controllers;

use App\Models\Repostaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RepostajeController extends Controller
{
  
  public function index()
   {
      $repostajes = DB::table('repostajes')->where('user_id', Auth::user()->id)->get();

      return view('repostajes.index', compact('repostajes'));
   }

  public function create($id = null)
   {
      return view('repostajes.create', compact('id'));
   }

  public function edit($id = null)
   {
      return view('repostajes.edit', compact('id'));  
   }  

  public function store(Request $request)
   {
      $datos = $request->all();

      $repostaje = Repostaje::create($datos);
      return redirect()->route('repostajes.index');
   }

  public function update(Request $request)
   {
      // Formar el array de datos
      $datos = $request->all();

      // Modificar el repostaje
      $repostaje = Repostaje::where('id', $datos['id'])->first();

      $repostaje->importe = $datos["importe"];
      $repostaje->litros_repostados = $datos["litros_repostados"];
      $repostaje->fecha_repostaje = $datos["fecha_repostaje"];
      $repostaje->save();
      
      return redirect()->route('repostajes.index');
   }

}