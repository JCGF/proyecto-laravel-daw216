<?php

namespace App\Http\Controllers;

use App\Models\Puntuacion;
use Illuminate\Http\Request;

class PuntuacionController extends Controller
{
  
  public static function insertarPuntuacion(Request $request)
   { 
      // Crear la fecha de la puntuacion
      $fechaP = date('Y-m-d H:i:s');  

      // Formar el array de datos
      $datos = $request->all();
      $datos['fecha_puntuacion'] = $fechaP;

      // Crear y guardar la puntuacion
      $puntuacion = Puntuacion::create($datos);
      return $puntuacion->id;
   }

   public static function modificarPuntuacion(Request $request)
   { 
      // Crear la fecha de la puntuacion
      $fechaP = date('Y-m-d H:i:s');  

      // Formar el array de datos
      $datos = $request->all();
      $datos['fecha_puntuacion'] = $fechaP;

      // Modificar la puntuacion
      $puntuacion = Puntuacion::where('user_id', $datos['user_id'])->where('gasolinera_id', $datos['gasolinera_id'])->first();
      $puntuacion->valor = $datos['valor'];
      $puntuacion->fecha_puntuacion = $datos['fecha_puntuacion'] = $fechaP;
      $puntuacion->save();

      return $puntuacion->id;
   }   

}
