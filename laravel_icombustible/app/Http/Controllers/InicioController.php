<?php

namespace App\Http\Controllers;

use App\Models\Gasolinera;
use Illuminate\Support\Facades\DB;

class InicioController extends Controller
{
  
  public function inicio()
   {
      return view('home');
   }

  public static function extraerGasolineras()
   {
      // Extraer todas las gasolineras de la BBDD
      $gasolineras = Gasolinera::all();

      return $gasolineras;
   }

  public static function extraerHorariosGasolineras()
   {
      // Extraer todos los horarios de la BBDD
      $horarios = DB::table('horarios')->orderBy('gasolinera_id', 'asc')->get();
      // Agrupar los datos por gasolinera_id
      $horarios = $horarios->groupBy('gasolinera_id');

      return $horarios;
   }

}
