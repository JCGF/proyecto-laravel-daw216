<?php

namespace App\Http\Controllers;

use App\Models\Gasolinera;
use Illuminate\Support\Facades\DB;

class GasolineraController extends Controller
{

  public function index()
   {
      //$gasolineras = Gasolinera::all();
      $gasolineras = Gasolinera::paginate(20);

      return view('gasolineras.index', compact('gasolineras'));
   }

  public function show(Gasolinera $gasolinera)
   {
			return view('gasolineras.show', ['gasolinera'=>$gasolinera]);
   }

  public static function extraerPreciosGasolinera($id_gasolinera)
   {
      // Extraer todos los precios de la Gasolinera seleccionada de la BBDD
      $precios = DB::table('precios')->where('gasolinera_id', '=', $id_gasolinera)->orderBy('combustible_id', 'asc')->get();

      return $precios;
   }

  public static function extraerComentariosGasolinera($id_gasolinera)
   {
      // Extraer todos los comentarios visibles de la Gasolinera seleccionada de la BBDD
      $comentarios = DB::table('comentarios')->where('gasolinera_id', '=', $id_gasolinera)->where('visible', '=', true)->orderBy('id', 'asc')->get();

      return $comentarios;
   }
   
  public static function numeroGasolineras()
   {
      return Gasolinera::all()->count();
   }

}
