# iCombustible

![alt text](images/iCombustible-logotipo.png "Logotipo de Icombustible"){width=150px height=150px}

### Pre-requisitos: 📋

- XAMPP (Servidores: Apache y MySQL)
- Composer
- Node.js / npm

#### _Instalación_

1. Composer:
- Acceder a [https://getcomposer.org/download/](https://getcomposer.org/download/) y en el apartado _Windows Installer_, descargar el archivo `Composer-Setup.exe`
- Ejecutar el archivo y seguir todo el proceso completo de instalación.
2. Node.js
- Acceder a [https://nodejs.org/es](https://nodejs.org/es) y en el centro de la página en el apartado _Descargar para Windows_ seleccionar la _versión Actual_ y descargarlo.
- Ejecutar el archivo y seguir todo el proceso completo de instalación.

### Iniciando la instalación ⚙️

Comenzaremos clonando el repositorio con Git utilizando el siguiente comando:

`git clone git@gitlab.com:JCGF/proyecto-laravel-daw216.git`

1. Composer:
- Nos posicionaremos en la ruta del proyecto, en mi caso en:  
`D:\xampp\htdocs\proyecto-laravel-daw216\laravel_icombustible` y procederemos a instalar los componente necesarios ejecutando el comando:
`composer install` 
2. Base de Datos:
- Crearemos la base de datos con el nombre de _icombustible_ y deberá usar la codificación _utf8mb4_unicode_ci_
- Procederemos a crear una copia del archivo de configuración _.env.example_ en nuestra carpeta del proyecto, ejecutando el siguiente comando:   
`cp .env.example .env`
- Editaremos el anterior archivo añadiendo el nombre de la base de datos, en el parámetro _DB_DATABASE_ resultando en: 
`DB_DATABASE=icombustible`
- Ahora procederemos a generar la clave aleatoria de nuestra aplicación de Laravel, ejecutando el comando:  
`php artisan key:generate`
- Por ultimo, procederemos a ejecutar las migraciones en nuestra aplicación y a generar los datos de prueba necesarios, procediendo a ejecutar el siguiente comando:  
`php artisan migrate:fresh --seed`
3. Storage:
- Para que el almacenamiento de imágenes funcione, deberemos editar el archivo _.env_ de nuestra aplicación añadiendole el siguiente parámetro: `FILESYSTEM_DRIVER=public`
- Por ultimo deberemos crear un enlace simbólico, ejecutando el comando:  
`php artisan storage:link`
4. Npm:
- Deberemos estar posicionados en la ruta del proyecto, en mi caso en:  
`D:\xampp\htdocs\proyecto-laravel-daw216\laravel_icombustible` y procederemos a instalar los paquetes necesarios mediante npm ejecutando el siguiente comando:  
`npm install`
- Por ultimo, iniciaremos el servidor de forma local, ejecutando el comando:  
`npm run dev` 

### Construido con: 🧰

- PHP 8 / HTML
- MySQL / phpMyAdmin
- Laravel 8
- Boostrap 5 / CSS
- JavaSript / jQuery 3.7
- Leaflet 1.9.4
- Git / Composer / Node.js / npm

### Autor: 👨🏻

Juan Carlos G.F - CFGS de Desarrollo de aplicaciones Web (DAW) 
